#!/usr/bin/python
from flask import Flask
import socket

app = Flask(__name__)


@app.route('/')
def hello_world():
    h = socket.gethostname()
    s =  '<h1>Welcome to {0}</h1>'.format(h)
    return s

if __name__ == '__main__':
    app.run(host='0.0.0.0')
